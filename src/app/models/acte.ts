import { Grup } from '../models/grup'

export class Acte{
  numero :Number;
  hora_inici :Date;
  hora_fi :Date;
  grup :Grup;
  constructor(numero=0,hora_inici=new Date(new Date(Date.now()).getTime()),hora_fi=new Date(new Date(Date.now()).getTime()),grup=new Grup()){
    this.numero=numero;
    this.hora_inici=hora_inici;
    this.hora_fi=hora_fi;
    this.grup=grup;
  }
  static createActes(){
    const grup1=new Grup("Los Gordos",8);
    const grup2=new Grup("Los Esparragos",9);
    const actesArray=[new Acte(0,new Date("March 25, 2015, 7:00:00 PM GMT+0"),new Date("March 25, 2015, 9:00:00 PM GMT+0"),grup1),new Acte(1,new Date("March 4, 1997, 11:03:00 AM GMT+0"),new Date("March 4, 1997, 12:23:00 AM GMT+0"),grup2),new Acte(2,new Date("March 4, 2000, 1:03:00 PM GMT+0"),new Date("March 4, 2000, 5:05:00 PM GMT+0"),grup1)];
    localStorage.setItem("Actes", JSON.stringify(actesArray));
  }
}
//para ver
//var storedNames = JSON.parse(localStorage.getItem("names"));
