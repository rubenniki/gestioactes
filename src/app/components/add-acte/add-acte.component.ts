import { Component, OnInit } from '@angular/core';
import { Acte } from '../../models/acte'
import { Grup } from '../../models/grup'

@Component({
  selector: 'app-add-acte',
  templateUrl: './add-acte.component.html',
  styleUrls: ['./add-acte.component.css']
})
export class AddActeComponent implements OnInit {
  acte :Acte
  actes :Acte[]
  alertMessage :string
  constructor() {
   }

  ngOnInit() {
    this.acte = new Acte();
    this.alertMessage=null
  }

  guardarActe(){
    this.actes=JSON.parse(localStorage.getItem("Actes"));
    if (this.actes) {
      var lastElement = this.actes.length-1;
      var acte = this.actes[lastElement];
      this.acte.numero= +acte.numero + 1
        this.actes.push(this.acte);
        localStorage.setItem("Actes", JSON.stringify(this.actes));
        this.alertMessage=" Acte Saved Correctly"
      }
      else{

      }
    }

  }
