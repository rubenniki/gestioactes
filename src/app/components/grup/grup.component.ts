import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Grup } from '../../models/grup'
import { Acte } from '../../models/acte'

@Component({
  selector: 'app-grup',
  templateUrl: './grup.component.html',
  styleUrls: ['./grup.component.css']
})
export class GrupComponent implements OnInit {
  @Input() grup :Grup;
  actes :Acte[];
  constructor(private route: ActivatedRoute) {
    this.actes=JSON.parse(localStorage.getItem("Actes"));
  }

  ngOnInit() {
   /*this.route.params.subscribe(params => {
      if (params['id'] != null) {
        this.grup = this.actes[params['id']].grup;
      }
    });*/
  }

}
