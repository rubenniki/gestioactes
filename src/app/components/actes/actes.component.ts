import { Component, OnInit } from '@angular/core';
import { Acte } from '../../models/acte';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actes',
  templateUrl: './actes.component.html',
  styleUrls: ['./actes.component.css']
})
export class ActesComponent implements OnInit {
  actes: String[]
  constructor(private router: Router) {
    if (localStorage.getItem("Actes")) {

    }
    else {
      if (localStorage.getItem("username")) {
        Acte.createActes();
      }
    }

  }

  ngOnInit() {
    if (localStorage.getItem("username")) {
      if (localStorage.getItem("Actes")) {
        this.actes = JSON.parse(localStorage.getItem("Actes"));

      }
    }else {
      this.router.navigate(['']);
    }
  }

}
