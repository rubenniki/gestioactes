import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Acte } from '../../models/acte'
import { Grup } from '../../models/grup'
import { GrupComponent } from '../grup/grup.component';

@Component({
  selector: 'app-acte',
  templateUrl: './acte.component.html',
  styleUrls: ['./acte.component.css']
})
export class ActeComponent implements OnInit {
  acte :Acte;
  actes :Acte[];
  id :Number;
  alertMessage :string;

  constructor(private route: ActivatedRoute) {
    this.actes=JSON.parse(localStorage.getItem("Actes"));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id'] != null) {
        this.id=params['id'];
        this.acte = this.actes[params['id']];
      }
    });
  }

  guardarActe(){
    this.actes=JSON.parse(localStorage.getItem("Actes"));
    if (this.actes) {
      this.actes[+this.id]=this.acte;
        localStorage.setItem("Actes", JSON.stringify(this.actes));
        this.alertMessage=" Acte Saved Correctly"
      }
      else{

      }
    }

}
