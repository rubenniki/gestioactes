import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  login: Boolean;
  logout: Boolean;
  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem("username")) {
      this.router.navigate(['actes']);
    }else{
      this.router.navigate(['']);
    }
  }

  ngDoCheck() {
    if (localStorage.getItem("username")) {
      this.login = false;
      this.logout = true;
    } else {
      this.logout = false;
      this.login = true;

    }
  }
}
