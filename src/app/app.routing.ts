import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ActesComponent } from './components/actes/actes.component';
import { ActeComponent } from './components/acte/acte.component';
import { GrupComponent } from './components/grup/grup.component';
import { AddActeComponent } from './components/add-acte/add-acte.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  {path:'actes',component: ActesComponent},
  {path:'acte/:id',component: ActeComponent},
  {path:'addActe',component: AddActeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting {}
